package me.yueli.project.pipe.entity;

import me.yueli.project.pipe.entity.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * @author hy
 */
@Entity
@Table(name = "subject", indexes = {
        @Index(name = "uk_account", columnList = "account", unique = true)
})
public class SubjectEntity extends BaseEntity<Long> {

    @Column(length = 16, nullable = false)
    private String nickname;

    @Column(length = 16, nullable = false)
    private String account;

    @Column(nullable = false)
    private String password;

    private String properties;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }
}
