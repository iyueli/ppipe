package me.yueli.project.pipe.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

/**
 * @author hqd 2021/12/21
 */
public abstract class Slf4jLogUtils {

    public static Logger getLogger(String name) {
        return LoggerFactory.getLogger(name);
    }

    public static Logger getLogger(Class<?> clazz) {
        return LoggerFactory.getLogger(clazz);
    }

    /**
     * info 日志
     *
     * @param logger    logger
     * @param msg       输出信息占位符使用 如 <code>xxx, {}, xxx, {}</code>
     * @param arguments 格式化参数，和占位符长度保持一致
     */
    public static void info(Logger logger, String msg, Object... arguments) {
        if (logger == null) {
            return;
        }

        if (logger.isInfoEnabled()) {
            logger.info(msg, arguments);
        }
    }

    /**
     * error 日志
     *
     * @param t         异常对象
     * @param logger    logger
     * @param msg       输出信息占位符使用 如 <code>xxx, {}, xxx, {}</code>
     * @param arguments 格式化参数，和占位符长度保持一致
     */
    public static void error(Throwable t, Logger logger, String msg, Object... arguments) {
        if (logger == null) {
            return;
        }

        if (logger.isErrorEnabled()) {
            logger.error(format(msg, arguments), t);
        }
    }

    /**
     * error 日志
     *
     * @param t      异常对象
     * @param logger logger
     */
    public static void error(Throwable t, Logger logger) {
        if (logger == null) {
            return;
        }

        if (logger.isErrorEnabled()) {
            logger.error("", t);
        }
    }

    /**
     * error 日志
     *
     * @param logger    Logger
     * @param msg       输出信息占位符使用 如 <code>xxx, {}, xxx, {}</code>
     * @param arguments 格式化参数，和占位符长度保持一致
     */
    public static void error(Logger logger, String msg, Object... arguments) {
        if (logger == null) {
            return;
        }

        if (logger.isErrorEnabled()) {
            logger.error(format(msg, arguments));
        }
    }

    /**
     * warn 日志
     *
     * @param t         异常对象
     * @param logger    logger
     * @param msg       输出信息占位符使用 如 <code>xxx, {}, xxx, {}</code>
     * @param arguments 格式化参数，和占位符长度保持一致
     */
    public static void warn(Throwable t, Logger logger, String msg, Object... arguments) {
        if (logger == null) {
            return;
        }

        if (logger.isWarnEnabled()) {
            logger.warn(format(msg, arguments), t);
        }
    }

    /**
     * warn 日志
     *
     * @param logger    logger
     * @param msg       输出信息占位符使用 如 <code>xxx, {}, xxx, {}</code>
     * @param arguments 格式化参数，和占位符长度保持一致
     */
    public static void warn(Logger logger, String msg, Object... arguments) {
        if (logger == null) {
            return;
        }

        if (logger.isWarnEnabled()) {
            logger.warn(format(msg, arguments));
        }
    }

    /**
     * 格式化
     *
     * @param pattern 模板
     * @param params  参数
     * @return
     */
    private static String format(String pattern, Object... params) {

        if (params == null) {
            return pattern;
        }
        return MessageFormatter.arrayFormat(pattern, params).getMessage();
    }

}
