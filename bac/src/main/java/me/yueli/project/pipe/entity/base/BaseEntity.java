package me.yueli.project.pipe.entity.base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author hy
 */
@MappedSuperclass
public class BaseEntity<ID extends Serializable> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "gmt_create", insertable = false, updatable = false, columnDefinition = "datetime not null default current_timestamp")
    private Date gmtCreate;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}
