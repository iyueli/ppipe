package me.yueli.project.pipe.iam;

import com.google.common.base.Splitter;
import me.yueli.project.pipe.context.RequestContext;
import me.yueli.project.pipe.controller.result.Result;
import me.yueli.project.pipe.util.CookieUtils;
import me.yueli.project.pipe.util.JacksonUtils;
import me.yueli.project.pipe.util.ResponseUtils;
import me.yueli.project.pipe.util.Slf4jLogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

/**
 * @author hy
 */
@WebFilter(urlPatterns = "/**")
@Component
public class SecurityFilter extends OncePerRequestFilter {

    private final List<String> whitelist;

    private final static Logger LOGGER = LoggerFactory.getLogger(SecurityFilter.class);

    @Autowired
    public SecurityFilter(@Value("${whitelist}") String whitelist) {
        this.whitelist = Splitter.on(',')
                .splitToList(whitelist);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            //is whitelist?
            String uri = request.getRequestURI();
            for (String path : whitelist) {
                if (path.equals(uri)) {
                    filterChain.doFilter(request, response);
                    return;
                }
            }

            //login?
            RequestContext requestContext = parseRequestContext(request);
            if (requestContext == null) {
                ResponseUtils.writeJSON(response, Result.fail("not login"));
                return;
            }

            RequestContext.setCurrentRequestContext(requestContext);

        } finally {
            RequestContext.clear();
        }
    }

    public RequestContext parseRequestContext(HttpServletRequest request) {
        try {
            Cookie cookie = CookieUtils.getCookie(request, RequestContext.TOKEN_NAME);

            if (cookie != null) {
                return JacksonUtils.object(new String(Base64.getDecoder().decode(cookie.getValue()), StandardCharsets.UTF_8), RequestContext.class);
            }
        } catch (Exception e) {
            Slf4jLogUtils.error(e, LOGGER, "SecurityFilter:parseRequestContext");
        }
        return null;
    }

}
