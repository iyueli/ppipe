package me.yueli.project.pipe.controller;

import me.yueli.project.pipe.context.RequestContext;
import me.yueli.project.pipe.controller.result.Result;
import me.yueli.project.pipe.entity.SubjectEntity;
import me.yueli.project.pipe.service.SubjectService;
import me.yueli.project.pipe.util.CookieUtils;
import me.yueli.project.pipe.util.JacksonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

/**
 * @author hy
 */
@RestController
public class LoginController {

    @Resource
    private SubjectService subjectService;

    @Value("${login.domain}")
    private String domain;

    @PostMapping("/login")
    public Result<SubjectEntity> login(HttpServletResponse response, @RequestBody SubjectEntity form) {
        SubjectEntity subject = subjectService.query(form.getAccount(), form.getPassword());
        if (subject == null) {
            return Result.fail("账号或者密码错误");
        }

        Cookie cookie = new Cookie(RequestContext.TOKEN_NAME, encodeLoginInfo(subject));

        cookie.setPath("/");
        cookie.setDomain(domain);

        response.addCookie(cookie);

        return Result.success("login success", subject);
    }

    @PostMapping("/logout")
    public Result<Void> logout(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = CookieUtils.getCookie(request, RequestContext.TOKEN_NAME);

        if (cookie != null) {
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }

        return Result.success("logout success");
    }

    private String encodeLoginInfo(SubjectEntity subject) {
        RequestContext context = new RequestContext();
        context.setSid(subject.getId());

        Optional.ofNullable(subject.getProperties())
                .map(JacksonUtils::map)
                .map(map -> (Boolean) map.getOrDefault("isAdmin", false))
                .ifPresent(context::setAdmin);

        return Base64.getEncoder().encodeToString(JacksonUtils.json(context).getBytes(StandardCharsets.UTF_8));
    }


}
