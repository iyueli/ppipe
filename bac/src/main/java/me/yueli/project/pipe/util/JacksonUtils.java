package me.yueli.project.pipe.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hy
 */
public class JacksonUtils {

    public static final ObjectMapper MAPPER = new ObjectMapper()
            //如果为空则不输出
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            //多余字段忽略
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            //视空字符传为null
            .enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

    public static String json(Object obj) {
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Object> map(String json) {
        return map(json, String.class, Object.class);
    }

    public static <K, V> Map<K, V> map(String json, Class<K> keyClass, Class<V> valueClass) {
        try {
            return MAPPER.readValue(json, MAPPER.getTypeFactory().constructMapType(HashMap.class, keyClass, valueClass));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T object(String json, Class<T> clazz) {
        try {
            return MAPPER.readValue(json, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
