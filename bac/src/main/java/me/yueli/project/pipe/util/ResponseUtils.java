package me.yueli.project.pipe.util;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author hy
 */
public class ResponseUtils {

    private static final Logger LOGGER = Slf4jLogUtils.getLogger(ResponseUtils.class);

    public static void writeJSON(HttpServletResponse response, Object object) {
        if (response.isCommitted()) {
            return;
        }
        response.setHeader("Content-type", "application/json;charset=UTF-8");

        response.setStatus(HttpStatus.OK.value());

        if (object != null) {
            try (PrintWriter writer = response.getWriter()) {
                writer.write(JacksonUtils.json(object));
            } catch (IOException e) {
                Slf4jLogUtils.error(e, LOGGER, "ResponseUtils:write:ex");
                throw new RuntimeException(e);
            }
        }
    }

}
