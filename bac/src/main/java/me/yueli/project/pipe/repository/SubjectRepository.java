package me.yueli.project.pipe.repository;

import me.yueli.project.pipe.entity.SubjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author hy
 */
@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, Long> {

    SubjectEntity queryByAccountAndPassword(String account, String password);

}
